I use these org-mode setting to publish to HTML:
```
(setq org-publish-project-alist
      '(

    ("website-shmip-html"
	 :base-directory "~/projects/hydro_intercomparsion/shmip-website-source"
	 :base-extension "org"
	 :publishing-directory "~/projects/hydro_intercomparsion/shmip-website-html"
	 :recursive t
;	 :publishing-function org-latex-publish-to-pdf ;org-html-publish-to-html
	 :publishing-function org-html-publish-to-html
	 :headline-levels 4             ; Just the default for this project.
	 :table-of-contents nil
	 :auto-preamble nil
	 :auto-sitemap nil                ; Generate sitemap.org automagically...
	 ;; :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
	 ;; :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
	 :sitemap-sort-folders "last"
         :style-include-scripts nil ;;https://lists.gnu.org/archive/html/emacs-orgmode/2011-01/msg00359.html
	 )

	("website-shmip-pdf"
	 :base-directory "~/projects/hydro_intercomparsion/shmip-website-source"
	 :base-extension "org"
	 :publishing-directory "~/projects/hydro_intercomparsion/shmip-website-pdf"
	 :recursive t
	 :publishing-function org-latex-publish-to-pdf
	 :headline-levels 4             ; Just the default for this project.
	 :table-of-contents nil
	 :auto-preamble nil
	 :auto-sitemap nil                ; Generate sitemap.org automagically...
	 ;; :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
	 ;; :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
	 :sitemap-sort-folders "last"
         :style-include-scripts nil ;;https://lists.gnu.org/archive/html/emacs-orgmode/2011-01/msg00359.html
	 )

	("website-shmip-static"
	 :recursive t
	 :base-directory "~/projects/hydro_intercomparsion/shmip-website-source"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|mp4\\|avi\\|ogg\\|ogv\\|webm|\\html|\\txt"
	 :publishing-directory "~/projects/hydro_intercomparsion/shmip-website-html"
	 :publishing-function org-publish-attachment
	 )

	("website-shmip" :components ("website-shmip-html" "website-shmip-static"))
)
```
