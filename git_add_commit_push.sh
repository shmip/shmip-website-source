#!/bin/sh
#
# Adds, commits and pushes.  Give commit message as argument.
#
# Build the HTML first with org-mode.

git commit -am "$1"
git push

PBB=../shmip-website-html/
cd $PBB
git add .
git commit -m "$1"
git push
