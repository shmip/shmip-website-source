#+SETUPFILE: ~/.emacs.d/org-templates/level-0-no-fold.org
#+OPTIONS: toc:nil
#+TITLE:   Subglacial Hydrology Model Inter-comparison Project @@latex:\\@@ @@html:<br>@@  SHMIP
#+DATE: <2014-05-30 Fri>

# http://orgmode.org/w/?p=org-mode.git;a=blob_plain;f=contrib/lisp/ox-bibtex.el;hb=HEAD


#+ATTR_HTML: :style float:right; margin: 0px 0px 20px 20px; width: 480px;
[[./docs/pic3.png]]

This project aims at providing a qualitative comparison of subglacial
hydrology models by comparing results from a suite of test runs.  It
is designed such that any subglacial hydrology model producing
effective pressure should be able to participate.

*Our publication is out:*
#+ATTR_HTML: :style float:left; margin: 0px 0px 20px 20px; width: 580px;
[[./docs/paper.png]]

http://dx.doi.org/10.1017/jog.2018.78


Quick links:
- [[./instructions.org][*instructions*]]
- [[file:technical-instructions.org][*technical details*]]
- [[file:news.org][*critical updates*]]

* Introduction

Over the last few years, a number of subglacial drainage system models
have been created and published.  However, as it is not known how
water actually flows at the ice-bed interface, it is far from clear
what physics to include in such models.  Consequently many different
types of drainage processes are included in these models.  A nice
overview over the main types of distributed drainage models is given
by Bueler & Pelt (2015).

This intercomparison project's *main aim* is to collect a set of model
runs which allows to *qualitatively compare the different model
behaviours*.  This should allow potential users of these models to
make a more a qualified decision as to which model to choose for their
application.  Likewise for model developers, this may help them to
assess which processes might be missing in their respective models.
Note that this intercomparison does not aim at assessing the
correctness of the participating models in any way.  In particular
this means that no attempt is made to ensure their correct numerical
implementation nor their applicability to a certain real world
scenario.  The former should probably be done by each model
individually whereas the latter might be the aim for a future hydro
intercomparison exercise.

Note that our aim is quite different from the numerous ice flow
intercomparison projects (e.g. Payne & al 2000).  In their case it is
reasonably clear what the physics are, at least for ice-flow, less so
of the boundary conditions.  This means that a one to one comparison
is possible, whereas here a more qualitative approach is necessary.

# * Participation

# The intercomparison is designed that many subglacial hydrology models
# should be able to participate.  The main restrictions are:
# - 1D or 2D
# - produces effective pressure as output

# *Detailed instructions* are [[./instructions.org][here]].

# *This is currently in a testing phase, until end of August 2016.* This
# means that we're looking for people willing to run the test cases with
# their models and provide feedback.  We will then adjust the
# test-suites accordingly before putting out a call for participation.

# #+CAPTION: Participators
# #+ATTR_HTML: :style float:left; width: 43em
# | Name               | Abbrev. | Model type/name  | Slow system | Efficient system | Dimensions |
# |--------------------+---------+------------------+-------------+------------------+------------|
# | Basile de Fleurian | ~bdef~  | Double continuum | yes         | yes              |         2D |
# | Mauro Werder       | ~mwer~  | GlaDS            | yes         | yes              |         2D |
# |                    |         |                  |             |                  |            |


* Meetings and discussions
1) [[./meeting1.org][IGS Chamonix 2014]]
2) [[./meeting2.org][AGU 2014]]
3) [[./docs/deFleurian_Hofn_2015.pdf][IGS Höfn 2015 (pdf of Basile's presentation)]]
4) [[./meeting3.org][Splinter meeting at EGU 2016 (kick-off)]]
5) [[./meeting5.org][Splinter meeting at EGU 2017 (first results)]]

* Contact
Basile de Fleurian: @@html:<script type="text/javascript">eval(unescape("%69%63%69%6e%66%36%34%3d%5b%27%25%34%32%25%36%31%25%37%33%25%36%39%25%36%63%25%36%35%25%32%65%25%34%34%25%36%35%25%34%36%25%36%63%25%36%35%25%37%35%25%37%32%25%36%39%25%36%31%25%36%65%27%2c%5b%27%25%36%65%25%36%66%27%2c%27%25%37%35%25%36%39%25%36%32%27%5d%2e%72%65%76%65%72%73%65%28%29%2e%6a%6f%69%6e%28%27%2e%27%29%5d%2e%6a%6f%69%6e%28%27%40%27%29%3b%6b%6d%6e%66%69%32%31%3d%75%6e%65%73%63%61%70%65%28%69%63%69%6e%66%36%34%29%3b%64%6f%63%75%6d%65%6e%74%2e%77%72%69%74%65%28%6b%6d%6e%66%69%32%31%2e%6c%69%6e%6b%28%27%6d%61%69%27%2b%27%6c%74%6f%3a%27%2b%69%63%69%6e%66%36%34%29%29%3b"));</script>@@

Mauro Werder: @@html:<script type="text/javascript">eval(unescape('%64%6f%63%75%6d%65%6e%74%2e%77%72%69%74%65%28%27%3c%61%20%68%72%65%66%3d%22%6d%61%69%6c%74%6f%3a%77%65%72%64%65%72%40%76%61%77%2e%62%61%75%67%2e%65%74%68%7a%2e%63%68%22%3e%77%65%72%64%65%72%40%76%61%77%2e%62%61%75%67%2e%65%74%68%7a%2e%63%68%3c%73%70%61%6e%20%63%6c%61%73%73%3d%22%69%63%6f%6e%22%3e%3c%2f%73%70%61%6e%3e%3c%2f%61%3e%27%29'))</script>@@


# * References
# - [[./docs/deFleurian_Hofn_2015.pdf][de Fleurian & al (2015, Höfn presentation)]]
# - [[http://www.geosci-model-dev.net/8/1613/2015/gmd-8-1613-2015.html][Bueler & Pelt (2015)]]
# - [[http://www.ingentaconnect.com/content/igsoc/jog/2000/00000046/00000153/art00007][Payne & al 2000]]
